import collections
from datetime import datetime
from bs4 import BeautifulSoup
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfinterp import resolve1
import pdftableextract
import requests


def get_pdf_urls(url):
    """
    Parses HTML with season PDF urls.

    :param url: season page URL
    :return: list with URLs
    """
    r = requests.get(url)
    assert r.status_code == 200, "Can't get page for a season {url}".format(url=url)

    soup = BeautifulSoup(r.text, 'html.parser')
    content = soup.find(id='parent-fieldname-text').find_all('a', class_='internal-link')
    return [a['href'] for a in content]


def get_season_page_urls(url):
    """
    Parses HTML with seasons list. Ignores seasons before 2013.

    :param url: main page URL
    :return: list with URLs
    """
    r = requests.get(url)
    assert r.status_code == 200, "Can't get homepage {url}".format(url=url)

    soup = BeautifulSoup(r.text, 'html.parser')

    content = soup.find(id='content-core').find_all('a', class_='summary url')

    res = []

    for a in content:
        print a.string
        start, end = map(int, a.string.split('-'))
        assert start == end - 1, "Can't parse dates: {text}".format(text=a.content)
        if start < 2013:
            continue

        res.append(a['href'])
    return res


def process_urls():
    """Entry point for URLs processing"""

    homepage = 'http://www.agricultura.gov.br/assuntos/sustentabilidade/agroenergia/acompanhamento-da-producao-sucroalcooleira'
    season_urls = get_season_page_urls(homepage)
    for url in season_urls:
        pdf_urls = get_pdf_urls(url)
        for pdf in pdf_urls:
            process_pdf_url(pdf)



def download_pdf(url):
    """
    Downoads PDF document.

    :param url: URL with PDF document
    :return: PDF content
    """
    r = requests.get(url)
    assert r.status_code == 200, 'Bad status code {code}'.format(code=r.code)
    return r.content


class LabeledValue(object):
    """Value wrapper with metadata."""

    def __init__(self, value, season, region, period_final, state, aspects):
        assert len(aspects) == 3, 'Invalid aspects list {aspects}'.format(aspects=aspects)
        self._value = value
        self._state = state.decode('utf-8')
        self._aspects = [it.decode('utf-8') for it in aspects]
        self._season = season
        self._region = region
        self._period_final = period_final

    def to_point(self):
        """Casts value to Shooju point."""
        return Point(datetime.strptime(self._period_final, "%d/%m/%Y"), self._value)

    def generate_fields(self):
        """
        Generates fields from metadata for given value.

        :return: dict with fields
        """
        return {
            "season": self._season,
            "region": self._region,
            "state": self._state,
            "aspect1": self._aspects[0],
            "aspect2": self._aspects[1],
            "aspect3": self._aspects[2],
        }

    def generate_sid(self):
        """
        Generates sid from metadata.

        :return: SID value
        """
        return datastore.sid(self._season, self._state, self._aspects[0], self._aspects[1], self._aspects[2])

    def __str__(self):
        return  '{value} {season} {region} {period_final} {state} {aspects}'.format(
            value=self._value, season=self._season, region=self._region,
            period=self._period_final, state=self._state, aspects=self._aspects)

    def __repr__(self):
        return str(self)


class RegionTableParser(object):
    """Utility class for table parsing."""

    SEASON_PREFIX = 'Safra:'
    REGION_PREFIX = 'Regi\xc3\xa3o: '
    PERION_FINAL_PREFIX = 'Per\xc3\xadodo final:'
    TOTAL_COLUMN = 'Tot.'

    def __init__(self, season, region, period_final):
        self.season = season
        self.region = region
        self.period_final = period_final
        self.table_id = (self.season, self.region, self.period_final)
        self._header_parsed=False

        self._subheaders = []
        self._data = []

        self._debug_rows = []

    @staticmethod
    def _parse_header_value(column, prefix):
        if column.startswith(prefix):
            return column[len(prefix):].strip()

    @classmethod
    def parse_table_header(cls, columns):
        """
        Parse a row and create parser instance if table header was found.

        :param columns: row columns
        :return: RegionTableParser if table header was found
        :return: None otherwise
        """
        if len(columns) < 4:
            return

        season = cls._parse_header_value(columns[0], cls.SEASON_PREFIX)
        if season is None:
            return

        region = None
        period_final = None

        for column in columns:
            if not region:
                region = cls._parse_header_value(column, cls.REGION_PREFIX)
            if not period_final:
                period_final = cls._parse_header_value(column, cls.PERION_FINAL_PREFIX)

        if not all([region, period_final]):
            raise ValueError('Invalid PDF table header: {header}'.format(header = columns))

        return RegionTableParser(season, region, period_final)

    def extend_table(self, table):
        """
        Add data from another partial table.

        :param table: a table parsed from another page
        """
        if table is None:
            return

        self._debug_rows = table._debug_rows + self._debug_rows
        self._data = table._data + self._data

    def parse_next_row(self, columns):
        """
        Parse next row from a page.

        :param columns: row data
        :return: True if page parsing completed
        :return: False otherwise
        """
        if not any(columns) or columns[0] == self.TOTAL_COLUMN:
            return True

        if len(self._subheaders) < 3:
            self._subheaders.append(self._parse_subheader(columns))
            return
        else:
            self._parse_data(columns)

        self._debug_rows.append(columns)

        return False

    def make_labeled_data(self):
        """
        Generates data with labels from parsed table.

        :return: list with labeled data
        """
        assert len(self._subheaders) == 3, 'Subheaders are not parsed yet'

        res = []
        for row in self._data:
            state = row[0]
            values = row[0:18]
            assert len(values) == 18, 'Invalid data row {row}'.format(row=values)

            for i, val in enumerate(values):
                if i == 0:
                    continue

                aspects = [header[i] for header in self._subheaders]
                res.append(LabeledValue(value=val, season=self.season, region=self.region,
                    period_final=self.period_final, state=state, aspects=aspects))

        return res

    def _parse_data(self, columns):
        self._data.append([it.replace('.', '') for it in columns])

    def _parse_subheader(self, columns):
        res = []
        last_value = None
        for i, name in enumerate(columns):
            if name:
                res.append(name)
                last_value = name
                continue

            if i != 0:
                assert last_value is not None, 'Bad table subheader ' + str(columns)
            res.append(last_value)

        return res


class PdfReader(object):
    """Utility class for PDF reading."""
    def __init__(self, pdf_path):
        self._pdf_path = pdf_path
        self._pages_count = None

    @property
    def pages_count(self):
        """Returns pages number in a file."""
        if self._pages_count is not None:
            return self._pages_count

        with open(self._pdf_path, 'rb') as f:
            parser = PDFParser(f)
            document = PDFDocument(parser)

            # This will give you the count of pages
            self._pages_count = resolve1(document.catalog['Pages'])['Count']
        return self._pages_count

    def read_all_pages(self, use_cache=False):
        """
        Read PDF data.

        :return: list with rows
        """
        pages = [str(i + 1) for i in xrange(self.pages_count)]

        cells = [pdftableextract.process_page(self._pdf_path, p) for p in pages]

        #flatten the cells structure
        cells = [item for sublist in cells for item in sublist ]
        # return cells

        #without any options, process_page picks up a blank table at the top of the page.
        li = pdftableextract.table_to_list(cells, pages)
        return li


class PagesParser(object):
    """Parses multiple pages and populates corresponding tables."""
    def __init__(self):
        self.tables = {}
        self._current = None

    def parse_page(self, rows, page_num, pages_count):
        """
        Parses single page with tables.

        :param rows: page's rows
        :param page_num: page number
        :param pages_count: pages total in PDF
        """
        tables_found = 0

        for row in rows:
            if not self._current:
                self._current = RegionTableParser.parse_table_header(row)
                if self._current:
                    tables_found += 1
                    self._current.extend_table(self.tables.get(self._current.table_id))
                    self.tables[self._current.table_id] = self._current
            else:
                is_done = self._current.parse_next_row(row)
                if is_done:
                    self._current = None

        if page_num != 0 and page_num != pages_count - 1:
            assert tables_found == 2, 'Bad tables number found on page[{id}]: {num}'.format(id=page_num, num=tables_found)


def submit_tables(tables):
    """
    Submits tables data to Shooju.

    :param tables: tables with data to submit
    """
    sid_to_data = collections.defaultdict(list)
    for table in tables:
        for value in table.make_labeled_data():
            sid_to_data[value.generate_sid()].append(value)
        print table.table_id

    for sid, values in sid_to_data.iteritems():
        # should be unique for given series id
        fields = values[0].generate_fields()

        points = [val.to_point() for val in values]
        datastore.put_fields(sid, {'source_obj': fields})
        datastore.put_points(sid, points)


def process_pdf_url(url):
    """
    Downloads, parses and submits data for given URL

    :param url: URL to process
    """
    logger.info('starting {}'.format(url))
    pdf_data = download_pdf(url)

    with tempfile.NamedTemporaryFile() as tf:
        tf.write(pdf_data)
        tf.flush()
        reader = PdfReader(tf.name)

        parser = PagesParser()
        pages = reader.read_all_pages(use_cache=False)
        for page_num, page_rows in enumerate(pages):
            parser.parse_page(page_rows, page_num, len(pages))

    submit_tables(parser.tables.values())
