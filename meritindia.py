import collections
from datetime import datetime, timedelta
import dateutil.parser
import json
from urlparse import urlparse, urljoin

import requests
from bs4 import BeautifulSoup


ValueUnits = collections.namedtuple('ValueUnits', ['value', 'units'])
LabelUnits = collections.namedtuple('LabelUnits', ['label', 'units', 'full_name'])


def parse_value(raw_value):
    if raw_value == '-':
        return None
    return float(raw_value.replace(',', ''))


def build_sid_from_values(*values):
        sid = [it.replace('@', '') for it in values]
        return datastore.sid(*sid)


class PortfolioParser(object):
    STATE_GENERATION = "STATE GENERATION"
    CENTRAL_ISGS = "CENTRAL ISGS"
    OTHER_ISGS = "OTHER ISGS"
    BILATERAL = "BILATERAL"
    POWER_EXCHANGE = "POWER EXCHANGE"

    def __init__(self, soup):
        self.soup = soup.find(id='div_Full_Gen')

    def parse_all_sections(self):
        sections = {}
        sections[self.STATE_GENERATION] = self._parse_state_generation()
        sections[self.CENTRAL_ISGS] = self._parse_central_isgs()
        sections[self.OTHER_ISGS] = self._parse_other_isgs()
        sections[self.BILATERAL] = self._parse_bilateral()
        sections[self.POWER_EXCHANGE] = self._parse_power_exchange()
        return sections

    @staticmethod
    def _get_title(soup):
        res = soup.find_all('div', class_='portfolio_title')
        assert len(res) == 1, 'Unexpected titles count'
        return res[0].text.strip()

    @staticmethod
    def _get_items(soup):
        return soup.find_all('div', class_='padding_none')

    @staticmethod
    def _get_label(soup):
        res = soup.find_all('div', class_='portfolio_sub_lable')
        assert len(res) == 1, 'Unexpected labels count'
        return res[0].get_text('\n', strip=True)

    @staticmethod
    def _get_value(soup):
        res = soup.find_all('div', class_='portfolio_sub_value')
        assert len(res) == 1, 'Unexpected values count'
        parts = res[0].get_text('\n', strip=True).split()

        if len(parts) == 1:
            return ValueUnits(value=None, units=parts[0])

        assert len(parts) == 2, 'Unexpected value parts count'
        return ValueUnits(value=parse_value(parts[0]), units=parts[1])

    def _parse_group(self, group_class, expected_title):
        soup = self.soup.find('div', class_=group_class)
        items = self._get_items(soup)
        title = self._get_title(soup)
        assert title == expected_title, 'Wrong title found "{wrong}" instead of "{expected}"'.format(
            wrong=title, expected=expected_title)
        content = {}
        for it in items:
            content[self._get_label(it)] = self._get_value(it)

        return content

    def _parse_state_generation(self):
        return self._parse_group('state_gen_data', self.STATE_GENERATION)

    def _parse_central_isgs(self):
        return self._parse_group('central_gen_data', self.CENTRAL_ISGS)

    def _parse_other_isgs(self):
        return self._parse_group('other_gen_data', self.OTHER_ISGS)

    def _parse_bilateral(self):
        return self._parse_group('bilateral_gen', self.BILATERAL)

    def _parse_power_exchange(self):
        return self._parse_group('pw_xchng_data', self.POWER_EXCHANGE)


class TableParser(object):
    def __init__(self, table_soup):
        self.header = self._parse_header(table_soup)
        self.rows = self._parse_rows(table_soup, self.header)

    @staticmethod
    def _parse_header(soup):
        header = []
        for column in soup.find('thead').find_all('th'):
            header.append(TableParser._parse_header_column(column))

        return header

    @staticmethod
    def _parse_header_column(column_soup):
        labels = column_soup.find_all('label')
        assert 0 < len(labels) < 3, 'Unexpected number of labels'

        if len(labels) == 1:
            label = labels[0].get_text('\n', strip=True)
            units = None
        else:
            label = labels[0].get_text('\n', strip=True)
            units = labels[1].get_text('\n', strip=True).lstrip('(').rstrip(')')

        full_name = ' '.join(column_soup.get_text('\n', strip=True).replace('\n', ' ').split())
        return LabelUnits(label=label, units=units, full_name=full_name)

    @staticmethod
    def _parse_rows(soup, header):
        rows = []
        for row in soup.find('tbody').find_all('tr'):
            rows.append(TableParser._parse_data_row(row, header))
            assert len(rows[-1]) == len(header), 'Incorrect row length'

        return rows

    @staticmethod
    def _parse_data_row(row, header):
        res = []
        for i, column in enumerate(row.find_all('td')):
            text = ' '.join([it.strip() for it in column.find_all(text=True)
                if 'display: none' not in it.parent.get('style', '')])
            text = text.strip()

            if header[i].units is None:
                res.append(text)
            else:
                res.append(parse_value(text))
        return res


class StationsParser(object):

    def __init__(self, soup):
        tables = soup.find(id='ShowDetailsForMerit').find_all('table')
        assert len(tables) == 2, "Can't find 2 tables on the page"
        self.table = TableParser(tables[0])


class TradeParser(object):

    def __init__(self, soup):
        table = soup.find(id='CompletedRequest_table')
        self.table = TableParser(table)


class PortfolioSeriesBuilder(object):
    TABLE = "Portfolio"

    @staticmethod
    def get_builders(state, timestamp, sections):
        res = []
        for section, data in sections.iteritems():
            for label, value_units in data.iteritems():
                if value_units.value is None:
                    continue
                res.append(PortfolioSeriesBuilder(state, timestamp, section, label, value_units))
        return res

    def __init__(self, state, timestamp, section, label, value_units):
        self.state = state
        self.timestamp = timestamp
        self.section = section
        self.label = label
        self.value_units = value_units

    def build_points(self):
        return [Point(self.timestamp,  self.value_units.value)]

    def build_fields(self):
        return {
            'table': self.TABLE,
            'state': self.state,
            'category': self.section,
            'aspect': self.label,
        }

    def build_sid(self):
        return build_sid_from_values(self.TABLE, self.state, self.section, self.label)


def find_index_in_header(header, name):
    for i, column in enumerate(header):
        if column.full_name == name:
            return i
    raise ValueError('Header name {name} not found'.format(name=name))


class StationsSeriesBuilder(object):
    TABLE = "Stations"

    STATION = u'Station'
    TYPE_OF_STATION = u'Type of Station'
    OWNERSHIP = u'Ownership'
    ASPECTS = [
        u'Plant Capacity (MW)',
        u'Capacity Allocated to State (MW)',
        u'Variable Cost (Rs/Unit)',
        u'Fixed Cost (Rs/Unit)',
        u'Total Cost (Rs/Unit)',
        u'Declared Capability (MWh)',
        u'Deviation from Merit Order (MWh)',
    ]

    @classmethod
    def get_builders(cls, state, timestamp, table):
        station_index = find_index_in_header(table.header, cls.STATION)
        type_of_station_index = find_index_in_header(table.header, cls.TYPE_OF_STATION)
        ownership_index = find_index_in_header(table.header, cls.OWNERSHIP)
        aspect_indices = {it: find_index_in_header(table.header, it) for it in cls.ASPECTS}
        res = []
        for row in table.rows:
            station = row[station_index]
            type_of_station = row[type_of_station_index]
            ownership = row[ownership_index]

            for aspect, index in aspect_indices.iteritems():
                value = row[index]
                if value is None:
                    continue
                res.append(StationsSeriesBuilder(state, timestamp, station, aspect, value, type_of_station, ownership))
        return res

    def __init__(self, state, timestamp, station, aspect, value, type_of_station, ownership):
        self.state = state
        self.timestamp = timestamp
        self.station = station
        self.aspect = aspect
        self.value = value
        self.type_of_station = type_of_station
        self.ownership = ownership

    def build_points(self):
        return [Point(self.timestamp,  self.value)]

    def build_fields(self):
        return {
            'table': self.TABLE,
            'station': self.station,
            'state': self.state,
            'aspect': self.aspect,
            'type_of_station': self.type_of_station,
            'ownership': self.ownership,
        }

    def build_sid(self):
        return build_sid_from_values(self.TABLE, self.state, self.station, self.aspect)


class TradeSeriesBuilder(object):
    TABLE = "Trade"
    DESCRIPTION = u'Description'
    ASPECTS = [
        u'Total Energy Purchased during the day (MWh)',
        u'Max Procurement Cost (Rs/Unit)',
        u'Min Procurement Cost (Rs/Unit)',
        u'Avg Procurement Cost (Rs/Unit)',
        u'Power Purchased at Max Rate during the Day (MW)',
    ]

    @classmethod
    def get_builders(cls, state, timestamp, table):
        description_index = find_index_in_header(table.header, cls.DESCRIPTION)
        aspect_indices = {it: find_index_in_header(table.header, it) for it in cls.ASPECTS}
        res = []
        for row in table.rows:
            description = row[description_index]

            for aspect, index in aspect_indices.iteritems():
                value = row[index]
                if value is None:
                    continue
                res.append(TradeSeriesBuilder(state, timestamp, description, aspect, value))
        return res

    def __init__(self, state, timestamp, description, aspect, value):
        self.state = state
        self.timestamp = timestamp
        self.description = description
        self.aspect = aspect
        self.value = value

    def build_points(self):
        return [Point(self.timestamp,  self.value)]

    def build_fields(self):
        return {
            'table': self.TABLE,
            'state': self.state,
            'description': self.description,
            'aspect': self.aspect,
        }

    def build_sid(self):
        return build_sid_from_values(self.TABLE, self.state, self.description, self.aspect)


def process_html(text, state, timestamp, submit_func):
    soup = BeautifulSoup(text, 'html.parser')

    portfolio_parser = PortfolioParser(soup)
    sections = portfolio_parser.parse_all_sections()

    stations_parser = StationsParser(soup)
    stations_table = stations_parser.table

    trade_parser = TradeParser(soup)
    trade_table = trade_parser.table

    builders = []
    builders += PortfolioSeriesBuilder.get_builders(state, timestamp, sections)
    builders += StationsSeriesBuilder.get_builders(state, timestamp, stations_table)
    builders += TradeSeriesBuilder.get_builders(state, timestamp, trade_table)

    for builder in builders:
        sid = builder.build_sid()
        points = builder.build_points()
        fields = builder.build_fields()
        submit_func(sid, points, fields)


def make_absolute_url(source_url, relative_url):
    parsed_uri = urlparse(source_url)
    domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
    url = urljoin(domain, relative_url)
    return url


class StateDatesFetcher(object):
    DATE_NAME = 'SelectedDateValue'
    FORM_ID = 'ResubmitForm'

    def __init__(self, start_url):
        self.start_url = start_url
        self.date_htmls = {}
        self.session = requests.Session()
        self.last_form = None
        self.last_action = None

        self._fetch_page(self.start_url)

        last_date = self._get_last_string_date()
        self.start_date = self._parse_string_date(last_date)

    @staticmethod
    def _parse_string_date(date):
        return dateutil.parser.parse(date)

    @staticmethod
    def _format_date_to_string(date):
        return date.strftime('%d %b %Y')

    def _get_last_string_date(self):
        return self.last_form[self.DATE_NAME]

    def _fetch_page(self, url):
        logger.info('processing main page at {url}'.format(url=url))
        r = self.session.get(url)
        assert r.status_code == 200, "Can't get url {url}".format(url=url)
        soup = BeautifulSoup(r.text, 'html.parser')

        self._setup_soup(soup)

    def fetch_page_for_date(self, timestamp):
        if timestamp in self.date_htmls:
            return True

        logger.debug('processing page for {timestamp}'.format(timestamp=timestamp))
        self.last_form[self.DATE_NAME] = self._format_date_to_string(timestamp)
        dst_url = make_absolute_url(self.start_url, self.last_action)
        r = self.session.post(dst_url, data=self.last_form)
        if r.status_code != 200:
            return False

        soup = BeautifulSoup(r.text, 'html.parser')
        self._setup_soup(soup)

        return True

    def _setup_soup(self, soup):
        form = self._get_form(soup)
        self.last_action, self.last_form = self._parse_form(form)

        last_date = self._get_last_string_date()
        parsed_date = self._parse_string_date(last_date)

        self.date_htmls[parsed_date] = soup

    def _post_data(action, data):
        dst_url = make_absolute_url(url, action)
        r = session.post(dst_url, headers=headers, data=values)

    @staticmethod
    def _parse_form(form_soup):
        action = form_soup['action']
        values = {}
        for it in form_soup.find_all('input'):
            values[it['name']] = it['value']

        return action, values

    @classmethod
    def _get_form(cls, soup):
        return soup.find(id=cls.FORM_ID)


class UrlFetcher(object):
    def __init__(self, main_url):
        self.main_url = main_url
        self.state_urls = self._parse_main_page(self.main_url)

    def _parse_main_page(self, url):
        parsed_uri = urlparse(url)
        domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)

        r = requests.get(url)
        assert r.status_code == 200, "Can't get homepage {url}".format(url=url)
        soup = BeautifulSoup(r.text, 'html.parser')
        rows = soup.find('tbody').find_all('tr')

        res = {}
        for row in rows:
            ref = row.find('a')
            state = ref.get_text('\n', strip=True)
            state_url = make_absolute_url(url, ref['href'])
            res[state] = state_url
        return res


    def get_all_htmls_for_state(self, state):
        fetcher = StateDatesFetcher(self.state_urls[state])
        curr_date = fetcher.start_date

        redo_days_count = max(0, settings.get('redo_days_count', 0))

        for i in xrange(redo_days_count):
            curr_date -= timedelta(days=1)
            fetcher.fetch_page_for_date(curr_date)

        return fetcher.date_htmls


def process_all_urls(main_url):
    max_dates = []
    urls_fetcher = UrlFetcher(main_url)
    for state in urls_fetcher.state_urls.iterkeys():
        logger.info('processing {state}'.format(state=state))
        date_htmls = urls_fetcher.get_all_htmls_for_state(state)
        max_dates.append(max(date_htmls.keys()))
        for timestamp, soup in date_htmls.iteritems():
            process_html(soup.prettify(), state, timestamp, submit_series)


def submit_series(sid, points, fields):
    datastore.put_fields(sid, {'source_obj': fields})
    datastore.put_points(sid, points)
