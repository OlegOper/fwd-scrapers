import collections
from datetime import datetime
from bs4 import BeautifulSoup
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfinterp import resolve1
import pdftableextract
import requests


class LabeledValue(object):
    """Value wrapper with metadata."""

    def __init__(self, value, from_field, to_field, rate):
        self._value = value
        self._from = from_field
        self._to = to_field
        self._rate = rate

    def generate_fields(self):
        """
        Generates fields from metadata for given value.

        :return: dict with fields
        """
        return {
            'from': self._from,
            'to': self._to,
            'value': self._value,
            'rate': self._rate,
        }

    def generate_sid(self):
        """
        Generates sid from metadata.

        :return: SID value
        """
        return datastore.sid(self._from, self._to, self._rate)

    def __str__(self):
        return '{_from} {to} {rate} {value}'.format(value=self._value, _from=self._from, to=self._to, rate=self._rate)


class TableParser(object):
    """Utility class for table parsing."""

    TABLE_HEADER = 'INTERNATIONAL JOINT TRANSPORTATION RATES IN US DOLLARS PER CUBIC METER'

    def __init__(self):
        self._table_found = False
        self._table_completed = False
        self._rates = []
        self._from_column = None
        self._data = []

    def parse_next_row(self, columns):
        """
        Parse next row from a page.

        :param columns: row data
        """
        if not self._table_found:
            self._table_found = any(columns) and columns[0] == self.TABLE_HEADER
            return

        self._table_completed = self._table_completed or not any(columns)
        if self._table_completed:
            return

        if not self._rates:
            self._parse_header(columns)
            return

        self._parse_data(columns)

    def make_labeled_data(self):
        """
        Generates data with labels from parsed table.

        :return: list with labeled data
        """
        return self._data

    def is_valid(self):
        """
        Check if a table was found

        :return: True if table was found
        :return: False otherwise
        """
        return self._table_found

    def _parse_header(self, columns):
        if any(columns[:2]):
            assert ['FROM', 'TO', 'RATE'] == columns[:3], 'Bad table header'
            return

        assert len(columns) > 3 and all(columns[2:]), 'Bad table rates header'

        self._rates = columns[2:]

    def _parse_data(self, columns):
        if columns[0]:
            self._from_column = columns[0]

        to_values = self._parse_to(columns[1])
        assert len(self._rates) == len(columns[2:]), 'Bad rates length'

        rates = {}
        for name, value in zip(self._rates, columns[2:]):
            if len(value) > 0:
                for to_column in to_values:
                    self._data.append(
                        LabeledValue(value=value, from_field=self._from_column, to_field=to_column, rate=name))

    def _parse_to(self, name):
        if ' or ' not in name:
            return [name]

        cities, state = name.split(',')
        return ['{city}, {state}'.format(city=city, state=state) for city in cities.split(' or ')]


class PdfReader(object):
    """Utility class for PDF reading."""
    def __init__(self, pdf_path):
        self._pdf_path = pdf_path
        self._pages_count = None

    @property
    def pages_count(self):
        """Returns pages number in a file."""
        if self._pages_count is not None:
            return self._pages_count

        with open(self._pdf_path, 'rb') as f:
            parser = PDFParser(f)
            document = PDFDocument(parser)

            # This will give you the count of pages
            self._pages_count = resolve1(document.catalog['Pages'])['Count']
        return self._pages_count

    def read_all_pages(self):
        """
        Read PDF data.

        :return: list with rows
        """
        pages = [str(i + 1) for i in xrange(self.pages_count)]

        cells = [pdftableextract.process_page(self._pdf_path, p) for p in pages]

        #flatten the cells structure
        cells = [item for sublist in cells for item in sublist ]
        # return cells

        #without any options, process_page picks up a blank table at the top of the page.
        li = pdftableextract.table_to_list(cells, pages)
        return li


class PagesParser(object):
    """Parses multiple pages and populates corresponding tables."""
    def __init__(self):
        self.tables = []

    def parse_page(self, rows, page_num, pages_count):
        """
        Parses single page with tables.

        :param rows: page's rows
        :param page_num: page number
        :param pages_count: pages total in PDF
        """
        table = TableParser()

        for row in rows:
            table.parse_next_row(row)

        if table.is_valid():
            self.tables.append(table)


def download_pdf(url):
    """
    Downoads PDF document.

    :param url: URL with PDF document
    :return: PDF content
    """
    r = requests.get(url)
    assert r.status_code == 200, 'Bad status code {code}'.format(code=r.code)
    return r.content


def process_pdf_url(url):
    """
    Downloads, parses and submits data for given URL

    :param url: URL to process
    """
    logger.info('starting {}'.format(url))
    pdf_data = download_pdf(url)

    with tempfile.NamedTemporaryFile() as tf:
        tf.write(pdf_data)
        tf.flush()
        reader = PdfReader(tf.name)

        parser = PagesParser()
        pages = reader.read_all_pages()
        for page_num, page_rows in enumerate(pages):
            parser.parse_page(page_rows, page_num, len(pages))

    submit_tables(parser.tables)


def submit_tables(tables):
    """
    Submits tables data to Shooju.

    :param tables: tables with data to submit
    """
    sid_to_data = collections.defaultdict(list)
    for table in tables:
        for value in table.make_labeled_data():
            sid_to_data[value.generate_sid()].append(value)

    for sid, values in sid_to_data.iteritems():
        # should be unique for given series id
        fields = values[0].generate_fields()

        datastore.put_fields(sid, {'source_obj': fields})


def process_urls():
    """Entry point for URLs processing"""

    url = 'http://www.enbridge.com/~/media/Enb/Documents/Tariffs/2018/EPI%20NEB%20423%20EELP%20FERC%2045140.pdf'
    process_pdf_url(url)
