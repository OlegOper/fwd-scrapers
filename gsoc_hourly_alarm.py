import collections
import datetime
import urlparse

from bs4 import BeautifulSoup
from dateutil import parser
import requests


# see https://github.com/requests/requests/issues/4046#issuecomment-302965044


import ssl
from requests.adapters import HTTPAdapter


FORCED_CIPHERS = (
    'ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+HIGH:'
    'DH+HIGH:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+HIGH:RSA+3DES'
)


class DESAdapter(HTTPAdapter):
    """
    A TransportAdapter that re-enables 3DES support in Requests.
    """
    def create_ssl_context(self):
        #ctx = create_urllib3_context(ciphers=FORCED_CIPHERS)
        ctx = ssl.create_default_context()
        # allow TLS 1.0 and TLS 1.2 and later (disable SSLv3 and SSLv2)
        ctx.options |= ssl.OP_NO_SSLv2
        ctx.options |= ssl.OP_NO_SSLv3
        ctx.set_ciphers(FORCED_CIPHERS)
        return ctx

    def init_poolmanager(self, *args, **kwargs):
        kwargs['ssl_context'] = self.create_ssl_context()
        return super(DESAdapter, self).init_poolmanager(*args, **kwargs)

    def proxy_manager_for(self, *args, **kwargs):
        kwargs['ssl_context'] = self.create_ssl_context()
        return super(DESAdapter, self).proxy_manager_for(*args, **kwargs)


def make_session():
    s = requests.Session()
    s.mount('https://', DESAdapter())
    return s


def _soup(content):
    return BeautifulSoup(content, "lxml")


class FormParser(object):
    def __init__(self, soup, name):
        self.soup = soup.find('form', {'name': name})
        self.action = self.soup['action']

    def get_inputs(self):
        res = {}
        for it in self.soup.find_all('input'):
            res[it['name']] = it.get('value', '')
        return res

    def get_post_url(self, form_url):
        return urlparse.urljoin(form_url, self.action)


class FormPoster(FormParser):
    def __init__(self, session, soup, name, form_url):
        self._session = session
        self._form_url = form_url
        super(FormPoster, self).__init__(soup, name)

    def post_form(self, form_data):
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
        }
        r = self._session.post(self.get_post_url(self._form_url), data=form_data, headers=headers)
        r.raise_for_status()

        return _soup(r.content)


class FormDate(object):
    def __init__(self, date):
        self.date = parser.parse(date).date()

    def decrease(self):
        self.date -= datetime.timedelta(days=1)

    def to_iso(self):
        return self.date.isoformat()

    def to_custom(self):
        return self.date.strftime('%b %d, %Y')


class TableParser(object):
    def __init__(self, soup):
        self.soup = soup.find('table', {'class': 'bgOuter'})
        self._date = FormDate(soup.find('input', {'id': 'hlyDetail:calendarDate'})['value'])

        headers = self._parse_headers()
        self.rows = self._parse_rows(headers)

    def is_empty(self):
        for vals in self.rows.itervalues():
            for column in vals.itervalues():
                if any(it is not None for it in column):
                    return False
        return True

    def _parse_headers(self):
        res = {}
        targets = ['NHCC', 'NH Spin', 'NH Supp']
        subtargets = ['25', '50', 'Final']
        for index, header in enumerate(self.soup.find_all('th', {'class': 'filesdisplayHeader'})):
            for t in targets:
                if t in header.text:
                    res[t] = index
                    break
        return res

    def _parse_rows(self, headers):
        soup = self.soup.find('table', {'class': 'tableDisplay'})
        res = {}
        for row in list(soup.find('tbody', recursive=False).find_all('tr', recursive=False)):
            columns = list(row.find_all('td', recursive=False))
            hour = int(columns[0].text)
            nhcc = columns[headers['NHCC']]
            spin = columns[headers['NH Spin']]
            supp = columns[headers['NH Supp']]
            res[hour] = {
                'nhcc': self._parse_column(nhcc, 1),
                'spin': self._parse_column(spin),
                'supp': self._parse_column(supp),
            }
        return res

    def _parse_column(self, column, start_subcolumn=0):
        subcols = list(column.find_all('td'))[start_subcolumn:][:3]
        return [self._parse_subcol(it) for it in subcols]

    def _parse_subcol(self, subcol):
        try:
            return float(subcol.find('a').text)
        except ValueError:
            return None


class RsrvReqParser(object):
    def __init__(self, soup):
        td = soup.find('table', {'class': 'bgOuter'}).find('td', {'class': 'vAlignTop'})
        table = list(td.find_all('table', recursive=False))[1]
        columns = list(table.find('table', {'class': 'tableDisplay'}).find_all('td'))
        self.spin = float(columns[0].text)
        self.supp = float(columns[1].text)


class AccountProcessor(object):
    def __init__(self, username, password):
        self._username = username
        self._password = password

        self._session = make_session()
        self._session.headers.update({
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',
        })

    def process(self, url):
        main_page = self._login(url)
        self._parse_page(main_page, url)

    def _parse_page(self, page, url):
        for i in xrange(500):
            table = TableParser(page)
            if table.is_empty():
                break
            form = FormParser(page, 'hlyDetail')
            date = FormDate(form.get_inputs()['hlyDetail:_id44'])
            rsrv_req = RsrvReqParser(page)
            self._submit_table(date.date, table, rsrv_req)
            page = self._goto_prev_page(page, url)

    def _submit_table(self, date, table, rsrv_req):
        series = {}
        daily = {
            'Spin Rsrv Req': [(date, rsrv_req.spin)],
            'Supp Rsrv Req': [(date, rsrv_req.supp)],
        }
        series['Daily'] = daily
        series['NHCC'] = self._prepare_table_results(date, table, 'nhcc')
        series['NH Spin'] = self._prepare_table_results(date, table, 'spin')
        series['NH Supp'] = self._prepare_table_results(date, table, 'supp')

        submit_series(series)

    def _prepare_table_results(self, date, table, field):
        names = ['25 Min', '50 Min', 'Final']
        res = collections.defaultdict(list)
        for hour, data in table.rows.iteritems():
            t = datetime.time(hour-1, 0)
            ts = datetime.datetime.combine(date, t)
            for name, value in zip(names, data[field]):
                point = (ts, value)
                res[name].append(point)
        return res

    def _goto_prev_page(self, page, url):
        form = FormPoster(self._session, page, 'hlyDetail', url)
        inputs = form.get_inputs()
        inputs['source'] = 'hlyDetail:_id39'
        date = FormDate(inputs['hlyDetail:_id44'])
        date.decrease()
        inputs['hlyDetail:calendarDate'] = date.to_iso()
        return form.post_form(inputs)

    def _login(self, url):
        r = self._session.get(url)
        soup = _soup(r.content)
        form = FormPoster(self._session, soup, 'Login', r.url)
        inputs = form.get_inputs()

        inputs['IDToken0'] = ''
        inputs['IDToken1'] = self._username
        inputs['IDToken2'] = self._password
        inputs['IDButton'] = 'Log In'

        form.post_form(inputs)

        r = self._session.get(url)
        soup = _soup(r.content)
        form = FormPoster(self._session, soup, 'security_check_form', r.url)
        inputs = form.get_inputs()
        soup = form.post_form(inputs)
        return soup
