import datetime
from dateutil.parser import parse
from suds.client import Client


class MessageFactory(object):
    def __init__(self, client):
        self.client = client

    def make_bin_sizes(self):
        return self.client.factory.create('ns2:BinSizes')

    def make_functions(self):
        return self.client.factory.create('ns2:Functions')

    def make_array_of_data_field(self, data_field_list):
        r = self.client.factory.create('ns2:ArrayOfDataField')
        r.DataField = data_field_list
        return r

    def make_data_field(self, field_name, function, hid):
        r = self.client.factory.create('ns2:DataField')
        r.FieldName = field_name
        r.Function.value = function
        r.HID = hid
        return r

    def make_field_info(self):
        return self.client.factory.create('ns2:FieldInfo')


class ApiStatusError(Exception):
    def __init__(self, message, code):
        self.message = '{}: {}'.format(code, message)
        self.code = code


class Api(object):
    def __init__(self, url):
        self.client = Client(url)
        self.port = self.client.service['BasicHttpBinding_WebAPI1']
        self.session_id = None

    def debug_print_last_messate(self):
        print self.client.last_sent()
        print self.client.last_received()

    @staticmethod
    def _check_status(response):
        code = response['Code']
        if code != 200:
            string_code = response['StringCode']
            raise ApiStatusError(string_code, code)

    @sjutils.retry()
    def _call(self, func, *args):
        return func(*args)

    def make_message_factory(self):
        return MessageFactory(self.client)

    def login(self, username, password):
        # Login(xs:string username, xs:string password, xs:string gatewayID, )
        r = self._call(self.port.Login, username, password, None)
        self._check_status(r)
        self.session_id = r['SessionID']
        return self.session_id

    def get_site_list(self):
        """The GetSiteList function returns a list of site IDs and Names."""
        # GetSiteList(xs:string sessionID, )
        r = self._call(self.port.GetSiteList, self.session_id)
        self._check_status(r)

        return r.Items.ListItem

    def get_site_hardware_list(self, site_id):
        """The GetSiteHardwareList function returns a list of information about all devices located on a site.
           This information is needed in order to query for data.
        """
        # GetSiteHardwareList(xs:string sessionID, xs:int siteID, )
        r = self._call(self.port.GetSiteHardwareList, self.session_id, site_id)
        self._check_status(r)

        return r.HardwareList.HardwareComplete

    def get_summary_hardware(self, site_id):
        """The GetSummaryHardware function returns information about a virtual summary device that exists
           for each site within PowerTrack. This device references daily summary data about the site
           such as total kWh produced and insolation. It provides easier and faster access when querying
           for total site production using one day or larger bins. Use the GetSummaryData function to query
           for data from the summary hardware.
        """
        # GetSummaryHardware(xs:string sessionID, xs:int siteID, )
        r = self._call(self.port.GetSummaryHardware, self.session_id, site_id)
        self._check_status(r)

        return r.HardwareList.HardwareComplete

    def get_bin_data(self, start_date, end_date, bin_size, fields):
        """The GetBinData function returns sample data from a list of devices and fields.
           Bins can range in size from 15 minute through 1 year.
        """
        # GetBinData(xs:string sessionID, xs:string fromLocal, xs:string toLocal, ns2:BinSizes binSize, ns2:ArrayOfDataField Fields, )
        r = self._call(self.port.GetBinData, self.session_id, self._date_to_string(start_date),
                self._date_to_string(end_date), bin_size, fields)
        self._check_status(r)

        return r.DataSet.DataBin

    @staticmethod
    def _date_to_string(d):
        return d.strftime('%m/%d/%Y')


def submit(bin_size, hardware_id, field_name, function, data):
    fields = {
        'bin_size': bin_size,
        'hardware_id': hardware_id,
        'field_name': field_name,
        'function': function,
    }
    sid = datastore.sid(bin_size, hardware_id, field_name, function)
    points = [Point(parse(k), v) for k, v in data.iteritems()]
    logger.info(str([sid, min(data.keys()), max(data.keys())]))

    datastore.put_fields(sid, {'source_obj': fields})
    datastore.put_points(sid, points)


def process_hardware(api, start_date, end_date, hardware):
    mf = api.make_message_factory()

    functions = mf.make_functions()
    target_functions = [functions.Avg, functions.Last, functions.Min, functions.Max, functions.Diff]

    bin_size = mf.make_bin_sizes().Bin15Min
    field_name = 'KWHnet'

    data_fields_list = []
    for function in target_functions:
        data_fields_list.append(mf.make_data_field(field_name, function, hardware.HardwareID))

    array_of_data_field = mf.make_array_of_data_field(data_fields_list)

    try:
        bin_data = api.get_bin_data(start_date, end_date, bin_size, array_of_data_field)
    except ApiStatusError as e:
        if e.code == 4004:
            params = {
                'code': e.code,
                'message': e.message,
                'hardware_id': hardware.HardwareID,
                'start_date': start_date,
                'end_date': end_date,
            }
            logger.debug('Skipping data bin [{hardware_id}, {start_date}, {end_date}]'
                        ' with code {code} because of {message}'.format(**params))
            return
        raise e

    for i, function in enumerate(target_functions):
        data = {it.Timestamp: it.Data.float[i] for it in bin_data}
        submit(str(bin_size), str(hardware.HardwareID), str(field_name), str(function), data)


def get_site_list(api):
    try:
        return api.get_site_list()
    except ApiStatusError as e:
        if e.code == 4004:
            logger.info("Can't get site list: {}".format(e.message))
            return []
        else:
            raise


def get_site_hardware_list(api, site_id):
    try:
        return api.get_site_hardware_list(site_id)
    except ApiStatusError as e:
        if e.code == 4004:
            logger.info("Can't get hardware list for siteID={}: {}".format(site_id, e.message))
            return []
        else:
            raise


def run(username, password, url, days_step, start_date):
    start_date = parse(start_date or '2018-01-01').date()

    step_size = datetime.timedelta(days=days_step)
    yesterday = datetime.date.today() - datetime.timedelta(days=1)

    if start_date >= yesterday:
        return

    api = Api(url)
    session_id = api.login(username, password)

    while start_date < yesterday:
        end_date = min(yesterday, start_date + step_size)
        logger.info('Processing [{start}:{end})'.format(start=start_date, end=end_date))

        for site in get_site_list(api):
            for hardware in get_site_hardware_list(api, site.ID):
                process_hardware(api, start_date, end_date, hardware)

        start_date = end_date
    return yesterday
